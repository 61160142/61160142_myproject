const userService = {
  userList: [
    { id: 1, name: 'Worawit', gender: 'M' },
    { id: 2, name: 'Thanapong', gender: 'M' },
    { id: 3, name: 'Napatwan', gender: 'F' }
  ],
  lastId: 3,
  addUser (user) {
    user.id = this.lastId++
    this.userList.push(user)
  },
  updateUser (user) {
    const index = this.userList.findIndex(item => item.id === user.id)
    this.userList.splice(index, 1, user)
  },
  deleteUser (user) {
    const index = this.userList.findIndex(item => item.id === user.id)
    this.userList.splice(index, 1)
  },
  getUsers () {
    return [...this.userList]
  }
}

export default userService
